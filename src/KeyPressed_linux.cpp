#include "KeyPressed_linux.h"

bool keys::is_pressed(KeySym ks) {
    Display *dpy = XOpenDisplay(":0");
    char keys_return[32];
    XQueryKeymap(dpy, keys_return);
    KeyCode kc2 = XKeysymToKeycode(dpy, ks);
    bool isPressed = !!(keys_return[kc2 >> 3] & (1 << (kc2 & 7)));
    XCloseDisplay(dpy);
    return isPressed;
}

bool keys::ctrl_is_pressed() {
    return is_pressed(XK_Control_L) || is_pressed(XK_Control_R);
}

bool keys::shift_is_pressed() {
    return is_pressed(XK_Shift_L) || is_pressed(XK_Shift_R);
}
