#ifndef UIELEMENT_H
#define UIELEMENT_H

#include <string>
//ui status flags in binary

#define COLORFLAG_HOVERED 0x1<<0
#define COLORFLAG_DRAGGING 0x1<<1
#define COLORFLAG_ISAGUIDE 0x1<<2
#define COLORFLAG_READOUT 0x1<<3

struct SimpleCoord{
    int x=0;
    int y=0;
};

struct Drag{
    //whether dragging or not
    bool ing=false;
    SimpleCoord start;
    SimpleCoord delta;
};

class UiElementCanvas{
    public:

    int x=0;
    int y=0;
    int x2=0;
    int y2=0;
    int w=0;
    int h=0;
    bool isInside(int wx, int wy){
        if(wx<x) return false;
        if(wy<y) return false;
        if(wx>x2) return false;
        if(wy>y2) return false;
        return true;
    }
};
class UiElement{
    public:
    //client has to teach UiElement how to draw
    static void circle (int x, int y, int radius);
    static void line (int x1, int y1, int x2, int y2);
    static void setColors (char colorFlags);
    static void text (int x, int y, const std::string str);
    static int getWindowWidth ();
    static int getWindowHeight ();
    static void pushMatrix ();
    static void popMatrix ();
    static void translate (int x, int y);

    UiElementCanvas canvas;


    virtual void setCanvas(int x1, int y1, int w, int h){
        canvas.x=x1;
        canvas.y=y1;
        canvas.x2=x1+w;
        canvas.y2=y1+h;
        canvas.w=w;
        canvas.h=h;
        canvasChanged();
    }
    virtual void setPosition(int x1,int y1){
        setCanvas(x1,y1,canvas.w,canvas.h);
    }
    virtual void canvasChanged(){}

    bool isMouseover=false;
    bool _wasMouseover=false;
    bool isDragging=false;
    bool isMiddleButtonDragging=false;
    SimpleCoord middleButtonDragStarted;

    virtual void draw(){
    }

    virtual void drawContour(){

        // std::cout<<"UiElement drawContour"<<std::endl;
        
        int inset=0;
        int x1 = canvas.x+inset;
        int x2 = canvas.x2-inset;
        int y1 = canvas.y+inset;
        int y2 = canvas.y2-inset;
        
        line(x1,y1,x2,y1);
        line(x1,y1,x1,y2);
        line(x2,y1,x2,y2);
        line(x1,y2,x2,y2);
    }

    //TODO: there might be need to differenciate mouse 
    //buttons in terms of "targeted" mouse events.
    //returns whether action taken
    virtual bool mouseDown(int x, int y, int button){
        if(isMouseover){
            switch (button){
            case 2:
                isMiddleButtonDragging=true;
                middleButtonDragStarted.x=x;
                middleButtonDragStarted.y=y;
                break;
            
            default:
                isDragging=true;
                break;
            }
            mouseTargetedDown(x,y,button);
            return true;
        }
        return false;
    }

    virtual bool mouseUp(int x, int y, int button){
        bool ret=false;
        if(isMouseover){
            mouseTargetedUp(x,y,button);
            ret=true;
        }else{
            if((isDragging||isMiddleButtonDragging)){
                mouseTargetEnd(x,y);
                ret=true;
            }
        }
        switch (button){
            case 2:
                isMiddleButtonDragging=false;
                break;
            
            default:
                isDragging=false;
                break;
        }
        return ret;
    }

    virtual bool mouseMove(int x, int y){
        if(canvas.isInside(x,y)){
            isMouseover=true;
            if(!_wasMouseover){
                mouseEnter(x,y);
                mouseTargetStart(x,y);
                _wasMouseover=true;
            }
            mouseTargetedMove(x,y);
            mouseOver(x,y);
            return true;
        }else{
            isMouseover=false;
            if(_wasMouseover){
                mouseLeave(x,y);
                if(!(isDragging||isMiddleButtonDragging)){
                    mouseTargetEnd(x,y);
                }
                _wasMouseover=false;
            }
            if((isDragging||isMiddleButtonDragging)){
                mouseTargetedMove(x,y);
                return true;
            }
        }
        return false;
    }

    //mouse moved on hover
    virtual void mouseOver(int x, int y){
        
    }
    virtual void mouseEnter(int x, int y){
        // std::cout<<friendlyName<<" mouse enter"<<std::endl;
    }
    virtual void mouseLeave(int x, int y){
        // std::cout<<friendlyName<<" mouse leave"<<std::endl;
    }

    //mouse moved while either hovered or dragging
    virtual void mouseTargetedMove(int x, int y){
        // std::cout<<"    "<<friendlyName<<" target move"<<std::endl;
    }
    //mouse interaction start with object detected
    virtual void mouseTargetStart(int x, int y){
        // std::cout<<friendlyName<<" target enter"<<std::endl;
    }
    //mouse interaction end with object ended (leave or stop dragging+leave)
    virtual void mouseTargetEnd(int x, int y){
        // std::cout<<"    "<<friendlyName<<" target leave"<<std::endl;
    }

    virtual void mouseTargetedDown(int x, int y, int button){}
    virtual void mouseTargetedUp(int x, int y, int button){}



};
#endif