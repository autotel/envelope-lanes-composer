#ifndef AUDIOMIDIINPUTCONTROLLER_H
#define AUDIOMIDIINPUTCONTROLLER_H
#include "./UiElement.h"

class AudioMidi: public UiElement{
    int width=300;
    int height=24;
    int margin=5;

    void setup(){
    }

    void place(int x, int y){
        setCanvas(x,y,width,height);
    }
    void canvasChanged(){

    }
    void draw(){
        drawContour();
        text(canvas.x+margin,canvas.y+margin,"midi input");
    }
};

#endif