// Refer to the README.md in the example's root folder for more information on usage
#include "ofApp.h"


void UiElement::circle(int x, int y, int radius){
	ofDrawCircle(x,y,radius);
}

void UiElement::line(int x1, int y1, int x2, int y2){
	ofDrawLine(x1,y1,x2,y2);
}

void UiElement::setColors(char colorFlags){
	if(colorFlags & COLORFLAG_DRAGGING){
		ofNoFill();
		ofSetHexColor(0x333333);
	}else if(colorFlags & COLORFLAG_HOVERED){
		ofSetHexColor(0xDD55FF);
		ofFill();
	}else  if(colorFlags & COLORFLAG_READOUT){
		ofSetHexColor(0x0);
		ofFill();
	}else {
		ofSetHexColor(0xCCCCCC);
		ofFill();
	}
}

void UiElement::text (int x, int y, const std::string str){
	ofDrawBitmapString(str, x, y);
}
int UiElement::getWindowWidth (){
	return ofGetWidth();
}
int UiElement::getWindowHeight (){
	return ofGetHeight();
}
void UiElement::pushMatrix (){
	return ofPushMatrix();
}
void UiElement::popMatrix (){
	return ofPopMatrix();
}
void UiElement::translate (int x, int y){
	return ofTranslate(x,y);
}


void ofApp::updateLanePositions(){
	

	window.bottomPanelTop = window.height - 100;
	
	int itemNo=0;

	int lastY = 0;
	for (auto const& iter : opr){
		// iter.second->setCanvas(0,itemNo*h/5,w,h/5);
		iter.second->setPosition(0,lastY);
		iter.second->fillWidth();
		lastY=iter.second->canvas.y2;
		++itemNo;
	}
};

//TODO: perhaps the audio out-putter is an operator
//therefore not needing this bull***t reference to mutex
ofApp::ofApp():ofBaseApp(),audioMutex(Operator::audioMutex){
	// audioMutex = &Operator::audioMutex;

	opr["env-1"]=new Envelope("osc-1's frequency");
	opr["env-2"]=new Envelope("osc-1's amplitude");
	opr["osc-1"]=new Oscillator("osc-1: fmodulator");

	// opr["env-3"]=new Envelope();
	opr["env-4"]=new Envelope("Osc-2's amp");
	opr["osc-2"]=new Oscillator("osc-2");

	opr["kernel"]=new Kernel("kernel");

	opr["env-1"]->pixelsPerLevel=0.2;
	opr["env-1"]->levelOffset = 500;

	opr["env-4"]->pixelsPerLevel=0.2;
	opr["env-4"]->levelOffset = 500;

	opr["env-2"]->pixelsPerLevel=100;
	opr["env-2"]->levelOffset = 1;

	// opr["env-3"]->pixelsPerLevel=0.2;
	// opr["env-3"]->levelOffset = 500;

	opr["env-1"]->connectTo(opr["osc-1"],"frequency");
	opr["env-2"]->connectTo(opr["osc-1"],"amplitude");

	opr["env-4"]->connectTo(opr["osc-2"],"amplitude");
	opr["osc-1"]->connectTo(opr["osc-2"],"frequency");

	opr["osc-2"]->connectTo(opr["kernel"],"input");
	opr["osc-1"]->connectTo(opr["kernel"],"kernel");


}

void ofApp::setup(){

	ofSetCircleResolution(10);
	ofBackground(255,255,255);

	ofSetWindowTitle("fields first concept");
	ofSetFrameRate(60); // if vertical sync is off, we can go a bit fast... this caps the framerate at 60fps.
	windowResized(ofGetWidth(),ofGetHeight());


	ofSoundStreamSettings settings;
	settings.numOutputChannels = 2;
	settings.sampleRate = 44100;
	settings.bufferSize = 512;
	settings.numBuffers = 4;
	settings.setOutListener(this);
	soundStream.setup(settings);

	timeIncrement=1000.00/(settings.sampleRate);

	std::cout << "HI " << endl;

	updateLanePositions();

}

float maxTime = 1000;

void ofApp::restart(){
	std::cout<<"restart"<<std::endl;
	unique_lock<mutex> lock(audioMutex);
	for (auto const& itr : opr){
		itr.second->audio_restart();
	}
	magicTime=0;
	playing=true;
}

float ofApp::getTime(){
	unique_lock<mutex> lock(audioMutex);
	return magicTime;
}

bool ofApp::getPlayingState(){
	unique_lock<mutex> lock(audioMutex);
	return playing;
}

void ofApp::audioOut(ofSoundBuffer &outBuffer) {

	bool bufIsPlaying=getPlayingState();
	if(!bufIsPlaying) return;
	
	float bufTime=getTime();
	float calculatedSample=0;

	
	int numFrames=outBuffer.getNumFrames();

	float intermediaryBuffer [numFrames]={0};

	std::unique_lock<std::mutex> lock(audioMutex);

	if(bufIsPlaying){
		opr["kernel"]->audio_fillBuffer(intermediaryBuffer,numFrames);
	}
	//TODO: find if theres a method to pass the whole buffer instead of one by one
	for(size_t i = 0; i < numFrames; i++) {
		if(magicTime >= maxTime) break;

		outBuffer.getSample(i, 0) = intermediaryBuffer[i];
		outBuffer.getSample(i, 1) = intermediaryBuffer[i];
	}

	if(playing) magicTime += timeIncrement * numFrames;

	if(magicTime >= maxTime){
		std::cout<<"end"<<std::endl;
		playing=false;
		magicTime=0;
	}
	lastBuffer = outBuffer;
}

void ofApp::update(){
}

void ofApp::draw(){

	for (auto const& itr : opr){
		itr.second->draw();
	}
	

}



void ofApp::keyPressed  (int key){
	switch (key)
	{
	case 0:
		if (keys::shift_is_pressed()) {  
			cout << "SHIFT" <<endl;
		}  
		break;
	
	case 32://space
		restart();
		cout << "space" << endl;
		break;
	
	default:
		cout << (float) key << endl;
		break;
	}
}


void ofApp::keyReleased  (int key){

}


void ofApp::mouseMoved(int x, int y ){

	//TODO:

	// forget about the dragging of windows,
	//just make it possible to "minimize"
	// and let them stay in place
	//minimization allows more operators to fit in the 
	//space.
	//later I can think on better guis, using some lib.
	//but I really want to explore the sonic aspects.

	//!keys::shift_is_pressed()
	for (auto const& itr : opr){
		if(itr.second->mouseMove(x,y)){};
	}

}


void ofApp::mouseDragged(int x, int y, int button){
	//!keys::shift_is_pressed()
	for (auto const& itr : opr){
		if(itr.second->mouseMove(x,y)){};
	}
}


void ofApp::mousePressed(int x, int y, int button){
	for (auto const& itr : opr){
		if(itr.second->mouseDown(x,y,button)) break;
	}

}


void ofApp::mouseReleased(int x, int y, int button){
	for (auto const& itr : opr){
		itr.second->mouseUp(x,y,button);
	}

	updateLanePositions();
}


void ofApp::mouseEntered(int x, int y){

}


void ofApp::mouseExited(int x, int y){

}

void ofApp::windowResized(int w, int h){

	window.width = w;
	window.height = h;
	updateLanePositions();
}


void ofApp::gotMessage(ofMessage msg){

}


void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
