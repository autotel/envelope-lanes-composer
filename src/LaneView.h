#ifndef LANEVIEW_H
#define LANEVIEW_H


#include <math.h>
#include "./Operator.h"
#include "./UiElement.h"

#define CAN_DRAG_LANES false


class LaneView: public Operator, public UiElement{
    public:


    class LaneHandle: public UiElement{
        public:
        LaneView & parent;
        LaneHandle(LaneView &_parent):parent(_parent){
            
        }
        void mouseTargetedMove(int x, int y){
            #if CAN_DRAG_LANES
            // std::cout<<"handle mouse t.move"<<std::endl;
            if(isDragging){
                parent.setPosition(canvas.x,y-30);
            }
            #endif
        }
        void draw(){
            if(isMouseover) setColors(COLORFLAG_HOVERED);
            // std::cout<<"LaneHandle draw"<<std::endl;
            for(int x=canvas.x+200; x<canvas.x2; x+=5){
                // std::cout<<"LaneHandle draw/iterator"<<std::endl;
                line(x,canvas.y,x+15,canvas.y2);
            }
            drawContour();

            // std::cout<<"LaneHandle draw end"<<std::endl;
            text(canvas.x+5, canvas.y+15, parent.friendlyName);
        }
    } laneHandle;
    
    LaneView():LaneView("Lane window"){
    };

    LaneView(const std::string _name):
        Operator(_name),laneHandle(*this)
    {

    };

    float pixelsPerTime = 1;//pixels per millisecond, for example
    float pixelsPerLevel = 1;
    float levelOffset = 0;

    float pixelsPerLevelWhenDragStarted=pixelsPerLevel;

    bool maximized_cached=false;
    bool maximized_target=true;

    int maximized_height = 200;
    int minimized_height = 0;


    void maximized_check(){

        // std::cout<<"LaneView maximized_check"<<std::endl;
        if(maximized_cached!=maximized_target){
            if(maximized_target){
                setCanvas(
                    canvas.x,canvas.y,
                    canvas.w,maximized_height
                );
            }else{
                setCanvas(
                    canvas.x,canvas.y,
                    canvas.w,minimized_height
                );
            }
            maximized_cached=maximized_target;
        }
    }

    //startTime is always zero from my point of view
    float endTime = 1000.00 * ((float) cacheSize / sampleRate);

    //set pixelsPerTime so that it fills the canvas width
    void fillWidth(){
        setCanvas(
            canvas.x,canvas.y,
            getWindowWidth(),canvas.h
        );
    }

    void setCanvas(int x1, int y1, int w, int h)override{
        int handleHeight=20;

        laneHandle.setCanvas(canvas.x,canvas.y,canvas.w,handleHeight);

        y1+=laneHandle.canvas.h;

        canvas.x=x1;
        canvas.y=y1;
        canvas.w=w;
        canvas.h=h;
        canvas.x2=x1+w;
        canvas.y2=y1+h;

        canvasChanged();
    }

    void canvasChanged()override{
        pixelsPerTime = (float)canvas.w/endTime;
    }

    float xToTime(int x){
        return (x - canvas.x) / pixelsPerTime;
    }
    float yToLevel(int y){
        return (y - canvas.y) / pixelsPerLevel - levelOffset;
    }
    int timeToX(float time){
        return (int) canvas.x + std::round(time * pixelsPerTime);
    }
    int levelToY(float val){
        return (int) canvas.y + std::round((val + levelOffset) * pixelsPerLevel);
    }

    unsigned long timeToSample(float time){
        return time  * sampleRate/1000.00;
    }

    virtual bool mouseMove(int x, int y){
        if(laneHandle.mouseMove(x,y)) return true;
        if(isMiddleButtonDragging){
            float yDist=middleButtonDragStarted.y-y;
            yDist/=100;
            float ratio = pow(2,yDist);
            pixelsPerLevel=pixelsPerLevelWhenDragStarted*ratio;
            return true;
        }
        return UiElement::mouseMove(x,y);
    }
    virtual bool mouseDown(int x, int y,int button)override{
        if(laneHandle.mouseDown(x,y,button)){
            maximized_target=!maximized_cached;
            return true;
        }
        if(button==2){
            pixelsPerLevelWhenDragStarted=pixelsPerLevel;
        }
        return UiElement::mouseDown(x,y,button);
    }
    virtual bool mouseUp(int x, int y,int button)override{
        if(laneHandle.mouseUp(x,y,button)) return true;
        return UiElement::mouseUp(x,y,button);
    }
    virtual void draw() override{
        if(isMouseover){    
            setColors(COLORFLAG_HOVERED);
        }else{
            setColors(COLORFLAG_READOUT);
        }
        laneHandle.draw();
        drawContour();
    }

    void drawContour() override {
        // std::cout<<"LaneView drawContour"<<std::endl;
        maximized_check();
        UiElement::drawContour();
    }

    void drawCache() {
        //TODO draw a fuzzy shape if frequency greater than pixels
        float prevx=0;
        float prevy=0;

        if(isMouseover){
            setColors(COLORFLAG_HOVERED);
        }else{
            setColors(COLORFLAG_ISAGUIDE);
        }

        
	    std::unique_lock<std::mutex> lock(audioMutex);
        unsigned long maxSampleNumber = cache.size();
        audio_syncCache();
        for(uint x = canvas.x; x<canvas.x2; x++){
            float time = xToTime(x);
            unsigned long sampleNumber = timeToSample(time);
            if(sampleNumber >= maxSampleNumber){
                line(x,canvas.y,x,canvas.y2);
                break;
            }
            float lev=cache[sampleNumber];
            int y = levelToY(lev);
            line(prevx,prevy,x,y);
            // cout<<lev<<"->"<<y<<'\t';
            prevx=x;
            prevy=y;
        }
        // cout<<endl;
    }
};


#endif