// Refer to the README.md in the example's root folder for more information on usage
#pragma once

#include "ofMain.h"
#include "./AudioMidi.h"
#include "./UiElement.h"
#include "./LaneView.h"
#include "./OperatorLanes/Envelope.h"
#include "./OperatorLanes/Oscillator.h"
#include "./OperatorLanes/Kernel.h"
#include "./KeyPressed_linux.h"
#include <unordered_map>

class ofApp : public ofBaseApp{
	
	struct{
		uint width=0;
		uint height=0;
		uint bottomPanelTop = 0;
	} window;

	uint grid(uint cols, uint colNumber){
		return (colNumber + 1) * window.width / (2 * cols);
	}
	
	AudioMidi audioMidiController;
	std::unordered_map<std::string,LaneView *> opr;

	public:
		ofApp();
		
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		void restart();
		void audioOut(ofSoundBuffer &outBuffer);
		float getTime();
		bool getPlayingState();

		void updateLanePositions();

		std::mutex& audioMutex;

		ofSoundStream soundStream;
		ofSoundBuffer lastBuffer;
		volatile bool playing=false;
		volatile float magicTime = 0;
		float timeIncrement;
};
	
