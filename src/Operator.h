#ifndef OPERATOR_H
#define OPERATOR_H

#include <unordered_set>
#include <map>
#include <iostream>
#include <mutex>
#include <vector>
struct Input;

/**
 * defines the base for all objects that produce an output of samples
 */
class Operator{

    // indicates whether the output needs to be calculated again,
    bool needsRecalculation=true;

    public:
    std::map<std::string, Input*> inputs;
    std::unordered_set<Input*> outputs;
    std::string friendlyName="operator";

    unsigned long cacheSize = 44100 * 2;
    // unsigned long cacheSize = 11025;
    std::vector<float> cache;
    
    static std::mutex audioMutex;
    static unsigned long sampleRate;
    static double samplePeriod;

    //set the flag to recalculate buffer
    //must be called every time some parameter changes
    //recalculation flag is propagated to all the outputs
    void markForRecalculation();
    void connectTo(Input *subject);
    void disconnect(Input *subject);
    void connectTo(Operator *subject,const std::string inputName);
    void disconnect(Operator *subject,const std::string inputName);

    Input* addInput(const std::string friendlyName);
    Operator(){}
    Operator(const std::string _name):friendlyName(_name){};
    ~Operator(){
        std::cout << "operator destroyed." << std::endl;
    };


    // Operator operator+(const Operator& subject){
    //     connectTo(subject);
    //     return this;
    // };
    // Operator operator-(const Operator& subject){
    //     disconnect(subject);
    //     return this;
    // };

    // all methods and vars starting with audio_ need to have the 
    // mutex locked before using, except from within another audio_
    // member

    unsigned long audio_bufferPosition=0;

    //updates the cache if the needsRecalculation flag is true
    void audio_syncCache(){
        if(needsRecalculation) audio_calculateCache();
    }
    //recalculates the cache
    void audio_calculateCache(){
        if(cache.size()<cacheSize) cache.resize(cacheSize);
        for(
            unsigned long sampleNumber=0;
            sampleNumber<cacheSize;
            ++sampleNumber
        ){
            cache[sampleNumber]=audio_calculateSample(sampleNumber);
        }
        needsRecalculation=false;
    }

    void audio_restart();

    virtual float audio_calculateSample(unsigned long sampleNumber){
        return 0;
    }
    
    virtual void audio_fillBuffer(float *buffer,int numFrames);
};


/**
 * input defines a structural input in an operator
 * it can only be connected to one output of other
 * operator (otherwise is nonesense, but mixing 
 * would be possible via a mixer operator)
 */
struct Input{
    Operator *myConnection=0;
    Operator &owner;
    const std::string friendlyName;
    Input(Operator &__owner,const std::string __friendlyName):
        friendlyName(__friendlyName),
        owner(__owner){
    }
    ~Input(){
        std::cout << "input "<<friendlyName<<" destroyed." << std::endl;
    }
    void connectedTo(Operator *subject);
    void disconnected(Operator *subject);
    float audio_calculateSample(unsigned long sampleNumber);
    float audio_getSample(unsigned long sampleNumber);

    friend Input* Operator::addInput(const std::string friendlyName);
};
#endif