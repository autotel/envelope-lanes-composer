#ifndef KERNEL_OPERATOR_H
#define KERNEL_OPERATOR_H

// #pragma once
#include "../LaneView.h"
#include "../Operator.h"
#include <deque>


//multiplies all last n samples by kernel samples
class Kernel: public LaneView{
    void __constr_common(){
        //the main input to affect
        mainInput=addInput("input");
        //the kernel that is sampled
        kernelInput=addInput("kernel");
    }
    public:

    //patcheable module related

    Input* mainInput;
    Input* kernelInput;

    Kernel():LaneView(){
        __constr_common();
    }

    Kernel(const std::string _name):LaneView(_name){
        __constr_common();
    };

    struct Edges{
        bool hovered=false;
        int radius=15;
        
        float start=0;//time
        float end=0;//time

        unsigned long startSample = 0;
        unsigned long endSample = 0;
        unsigned long sampleLength = 0; 

        bool draggingStart = false;
        bool draggingEnd = false;

        bool hoveringStart = false;
        bool hoveringEnd = false;

        void syncLength(){
            sampleLength=endSample-startSample;
        }
    }edges;

    bool isStartEdgeUnder(int x, int y){
        int startx = timeToX(edges.start);
        return(
            startx - edges.radius < x &&
            startx + edges.radius > x
        );
    }
    bool isEndEdgeUnder(int x, int y){
        int endx = timeToX(edges.end);
        return(
            endx - edges.radius < x &&
            endx + edges.radius > x
        );
    }

    void setRange(float edgea, float edgeb){
        if(edgea>edgeb){
            setStartTime(edgeb);
            setEndTime(edgea);
        }else{
            setStartTime(edgea);
            setEndTime(edgeb);
        }
    }
    void setStartTime(float to){
        edges.startSample=timeToSample(to);
        edges.start=to;
        edges.syncLength();
    };
    void setEndTime(float to){
        edges.endSample=timeToSample(to);
        edges.end=to;
        edges.syncLength();
    };

    

    //output related
    unsigned long lastSample = 0;

    std::deque<float> nInputSamples;

    void addInputSample(float insample){
        nInputSamples.push_back(insample);
        while(nInputSamples.size()>edges.sampleLength){
            nInputSamples.pop_front();
        }
    }
    
    float audio_calculateSample(unsigned long sampleNumber){

        if(!mainInput) return 0;
        if(!kernelInput) return 0;

        if(sampleNumber==0){
            lastSample=0;
            nInputSamples.clear();
        }

        //should always be 1
        //although kernels now can request a sample in looped order
        //the calculation is however cached, thus call should always be
        //one by one.
        //maybe I should remove the "sampleNuber" parameter and assume it's
        //always previous+1?

        float inputSpl = mainInput->audio_getSample(sampleNumber);

        addInputSample(inputSpl);

        float result = 0;

        std::deque<float>::iterator delayedSampleIterator = nInputSamples.end();
        float delayedInputSpl = inputSpl;
        
        //mult each last n sample by each kernel sample
        for(unsigned long inSampleN = 0; inSampleN < edges.sampleLength; inSampleN++){
            unsigned long kernelSampleN = inSampleN + edges.startSample;
            float kernelSpl = kernelInput->audio_getSample(kernelSampleN);
            float partialResult = kernelSpl * delayedInputSpl;
            // if(partialResult>3){
            //     std::cout<<"strange sample:"<<partialResult<<"\tinSpl"<<inSampleN<<"\tKSpl"<<kernelSampleN<<std::endl;
            // }
            result += partialResult / (float) edges.sampleLength;
            --delayedSampleIterator;
            if(delayedSampleIterator == nInputSamples.begin()) break;
            delayedInputSpl=*delayedSampleIterator;
            // result += partialResult;
        }

        lastSample=sampleNumber;
        
        return result;
    }

    //gui related
    struct {
        int x=0;
        int y=0;
        float time=0;
        float level=0;
        bool display=false;
    } hoverPlace;

    void updateHoverPlace(int x, int y){
        float mouseTime = xToTime(x);
        
        unsigned long mouseSampleN = timeToSample(mouseTime);
        
        long maxSample = cache.size()-1;
        if(maxSample<0) return;
        if(mouseSampleN>maxSample) mouseSampleN=maxSample;

        hoverPlace.time=mouseTime;
        hoverPlace.level=cache[mouseSampleN];
        hoverPlace.x=x;
        hoverPlace.y=levelToY(hoverPlace.level);
    }


    void mouseTargetedMove(int x, int y)override{
        //project the mouse coords to real values
        float mouseTime = xToTime(x);
        float mouseLevel = yToLevel(y);

        updateHoverPlace(x,y);

        if(isDragging){
            if(edges.draggingEnd){
                setRange(edges.start,mouseTime);
            }else if(edges.draggingStart){
                setRange(edges.end,mouseTime);
            }
            markForRecalculation();
        }else{

            edges.hoveringEnd=false;
            edges.hoveringStart=false;
            if(isEndEdgeUnder(x,y)){
                edges.hoveringEnd=true;
            }else if(isStartEdgeUnder(x,y)){
                edges.hoveringStart=true;
            }
        }
    }

    void mouseTargetedDown(int x, int y, int button)override{
        LaneView::mouseTargetedDown(x,y,button);
        if(edges.hoveringEnd){
            edges.draggingEnd=true;
        }else if(edges.hoveringStart){
            edges.draggingStart=true;
        }
    }

    void mouseTargetedUp(int x, int y, int button)override{
        LaneView::mouseTargetedUp(x,y,button);
        edges.draggingEnd=false;
        edges.draggingStart=false;
    }

    void setCanvas(int x1, int y1, int w, int h)override{
        LaneView::setCanvas(x1,y1,w,h);

        pixelsPerLevel = canvas.h / 2.00;
        levelOffset = 1;
    }
    void mouseTargetStart(int x, int y){
        markForRecalculation();
    }

    void draw()override{
        LaneView::draw();

        if(!maximized_cached) return;

        drawCache();

        int startx = timeToX(edges.start);
        int endx = timeToX(edges.end);

        // TODO: make this code less stupid. I am too hungry now
        if(edges.draggingStart){ setColors(COLORFLAG_DRAGGING); }
        else if(edges.hoveringStart){ setColors(COLORFLAG_HOVERED); }
        else { setColors(COLORFLAG_ISAGUIDE); }
        line(startx,canvas.y,startx,canvas.y2);
        // line(startx+2,canvas.y,startx+2,canvas.y2);

        if(edges.draggingEnd){ setColors(COLORFLAG_DRAGGING); }
        else if(edges.hoveringEnd){ setColors(COLORFLAG_HOVERED); }
        else { setColors(COLORFLAG_ISAGUIDE); }
        line(endx,canvas.y,endx,canvas.y2);
        // line(endx+2,canvas.y,endx+2,canvas.y2);

    }
};

#endif