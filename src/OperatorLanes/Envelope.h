#ifndef ENVELOPE_FIELD_H
#define ENVELOPE_FIELD_H

#include <vector>
#include <iostream>
#include <algorithm>
#include "../LaneView.h"
#include "../Operator.h"

struct EnvPoint{
    
    bool hovered = false;

    //pixels
    //todo: this and calls to this should be static
    int radius = 5;

    struct EnvDrag:public Drag{
        //flag indicating axes that should be dragged
        char axes = 0b11;
    }drag;

    //units
    float time = 0;
    float level = 0;

    EnvPoint(){}
    EnvPoint(float t, float l): time(t),level(l){}
    ~EnvPoint(){
        std::cout<<"point destroyed"<<std::endl;
    }

    char getFlags(){
        char ret=0;
        if(drag.ing) ret |= COLORFLAG_DRAGGING;
        if(hovered) ret |= COLORFLAG_HOVERED;
        return ret;
    }

};

class Envelope: public LaneView{
    public:

    std::vector<EnvPoint*> envPoints;

    Drag drag;

    bool needRedraw=true;

    float pixelsPerLevel = 1;

    struct {
        int x=0;
        int y=0;
        float time=0;
        float level=0;
        bool display=false;
    } hoverPlace;

    Envelope():LaneView(){}

    Envelope(const std::string _name):LaneView(_name){
    };

    EnvPoint * addPoint(float time, float level){
        EnvPoint * newPoint = new EnvPoint();
        if(!newPoint) return 0;
        newPoint->time=time;
        newPoint->level=level;
        envPoints.push_back(newPoint);
        sortPointsByTime();
        needRedraw=true;
        return newPoint;
    }

    void deletePoint(EnvPoint * subject){
        //erase-remove idiom
        //efficiently removes elements from array
        envPoints.erase(
            std::remove(
                envPoints.begin(), envPoints.end(), subject
            ), envPoints.end()
        );
    }

    void sortPointsByTime(){
        std::sort(
            std::begin(envPoints),
            std::end(envPoints),
            [](auto i,auto j){
                return i->time<j->time;
            }
        );
    }

    EnvPoint * coordHasEnvPoint(int x, int y){
        for(auto it = std::begin(envPoints);
            it != std::end(envPoints);
            ++it
        ) {
            if((*it)->hovered){
                //TODO: test that this actually 
                //returns a correct pointer
                return *it;
            }
        }
        return 0;
    }

    void releaseAllDragging(){
        for(auto it = std::begin(envPoints);
            it != std::end(envPoints);
            ++it
        ) {
            (*it)->drag.ing=false;
        }
        drag.ing=false;
        needRedraw=true;
    }
    //TODOS:
    // - when mouse down on the lower part, create a point at
    //   that time but exactly at level that would cause no changes,
    //   such that it's a line slicer (use text)
    // - modifier key lets adjust with more precision, slower movement
    // - find way to let mouse move unlimited amt up or down while dragging
    void mouseTargetedDown(int x, int y, int button)override{
        drag.ing=true;
        drag.start.x=x;
        drag.start.y=y;
        EnvPoint * pointUnderMouse = coordHasEnvPoint(x,y);
        if(pointUnderMouse){
            pointUnderMouse->drag.ing=true;
            pointUnderMouse->drag.start.x=x;
            pointUnderMouse->drag.start.y=y;
            if(button==1){
                deletePoint(pointUnderMouse);
                pointUnderMouse=0;
                return;
            }
        }else{
            
            EnvPoint * created = addPoint(
                xToTime(x),
                yToLevel(y)
            );
            if(!created) {
                std::cout<<"no point created"<<std::endl;
                return;
            }
            releaseAllDragging();
            //TODO: following lines not taking effect ¿?
            //makes new points get dragged once created
            created->drag.ing=true;
            created->drag.start.x=x;
            created->drag.start.y=y;
            created->drag.axes=0b11;

        }
    }
    
    bool mouseUp(int x, int y, int button)override{
        bool ret=LaneView::mouseUp(x,y,button);
        releaseAllDragging();
        markForRecalculation();
        return ret;
    }

    void mouseTargetStart(int x, int y)override{
        hoverPlace.display=true;
    }
    void mouseTargetEnd(int x, int y)override{
        hoverPlace.display=false;
    }
    void updateHoverPlace(int x, int y){
        float mouseTime = xToTime(x);
        
        unsigned long mouseSampleN = timeToSample(mouseTime);
        
        long maxSample = cache.size()-1;
        if(maxSample<0) return;
        if(mouseSampleN>maxSample) mouseSampleN=maxSample;

        hoverPlace.time=mouseTime;
        hoverPlace.level=cache[mouseSampleN];
        hoverPlace.x=x;
        hoverPlace.y=levelToY(hoverPlace.level);


        

    }
    //TODO: update these inner workings to reflect new
    //mouse enter and mouse leave detectors now present
    //in LaneView
    void mouseTargetedMove(int x, int y)override{
        //only allow one point to get hovered
        bool onlyOneHover = true;
        bool alreadyHoveredOne = false;
        //project the mouse coords to real values
        float mouseTime = xToTime(x);
        float mouseLevel = yToLevel(y);

        updateHoverPlace(x,y);

        drag.delta.x = x - drag.start.x;
        drag.delta.y = x - drag.start.y;


        if(drag.ing){
            //TODO:drag the points, but not just to mouse position
            //but relative to where mouse was pressed
            for(
                auto it = std::begin(envPoints);
                it != std::end(envPoints);
                ++it
            ) {
                EnvPoint * its = *it;
                if(its->drag.ing){
                    if(its->drag.axes & 0b01) its->time = mouseTime;
                    if(its->drag.axes & 0b10) its->level = mouseLevel;
                    sortPointsByTime();
                    markForRecalculation();
                }
            }
        }else{
            for(
                auto it = std::begin(envPoints);
                it != std::end(envPoints);
                ++it
            ){
                EnvPoint * its = *it;
                int radius = its->radius;
                int pointx =  timeToX(its->time);
                int pointy =  levelToY(its->level);

                bool grabbedByVertex = 
                    pointx - radius < x &&
                    pointx + radius > x &&
                    pointy - radius < y &&
                    pointy + radius > y;
                
                bool grabbedBySeparator = 
                    pointx - radius < x &&
                    pointx + radius > x &&
                    canvas.y2 - 2*radius < y &&
                    canvas.y2 + 2*radius > y;

                its->drag.axes = grabbedByVertex << 1 | grabbedBySeparator;

                // if(precise) its->drag.axes=0b11;

                if(grabbedByVertex || grabbedBySeparator){
                    if(onlyOneHover){
                        if(!alreadyHoveredOne){
                            its->hovered=true;
                            alreadyHoveredOne=true;
                        }
                    }else{
                        its->hovered=true;
                    }

                    // if(grabbedByVertex){
                    //     its->drag.axes=0b11;
                    // }else{
                    //     //only let x move
                    //     its->drag.axes=0b1;
                    // }

                    markForRecalculation();
                }else{
                    its->hovered=false;
                }
            }
        }

    }
    //TODO: this can be optimized when it's run for a range of time
    //because it searches for neighbor points on each time
    float audio_calculateSample(unsigned long sampleNumber)override{
        float time = (float)1000 * sampleNumber/sampleRate;
        //should never be out of order, but just in case
        sortPointsByTime();
        //first point after time
        EnvPoint * rightBefore = 0;
        EnvPoint * rightAfter = 0;
        //if nonexistent, use zero
        float rightAfterTime = 0;
        float rightAfterLevel = 0;
        float rightBeforeTime = 0;
        float rightBeforeLevel = 0;
        //get time and level of next point
        for(
            auto it = std::begin(envPoints);
            it != std::end(envPoints);
            ++it
        ) {
            EnvPoint * its = *it;
            if(its->time > time){
                rightAfter=its;
                rightAfterLevel = its->level;
                rightAfterTime = its->time;
                break;
            }
            rightBefore=its;
        }
        //last point before time is the prev one.
        //if nonexistent, use zero
        //get time and level of prev point
        if(rightBefore){
            rightBeforeTime=rightBefore->time;
            rightBeforeLevel=rightBefore->level;
        }
        //interpolate weighted by distance to either.
        //weights are not ratios, 
        //instead they range from zero to before-after interval
        float totalWeight = rightAfterTime - rightBeforeTime;
        float prevWeight = rightAfterTime - time;
        float nextWeight = time - rightBeforeTime;
        return (
            rightBeforeLevel * prevWeight + 
            rightAfterLevel * nextWeight
        )/totalWeight;
    }
    
    void draw()override{
        LaneView::draw();

        if(!maximized_cached) return;

        if(!needRedraw) return;

	    std::unique_lock<std::mutex> lock(audioMutex);

        audio_syncCache();

        bool isFirst=true;
        SimpleCoord prev;

        int centerY=levelToY(0);
        setColors(COLORFLAG_ISAGUIDE);
        line(canvas.x,centerY,canvas.x2,centerY);
        
        for(
            auto it = std::begin(envPoints);
            it != std::end(envPoints);
            ++it
        ) {
            EnvPoint * its = *it;
            int pointx =  timeToX(its->time);
            int pointy =  levelToY(its->level);

            setColors(its->getFlags());
            if(canvas.isInside(pointx,pointy)){
                circle(pointx,pointy,its->radius);
            }
            if(its->hovered){
                line(pointx,canvas.y,pointx,canvas.y2);
            }
            circle(
                pointx,
                canvas.y2-its->radius,
                its->radius
            );
            if(!isFirst){
                line(prev.x,prev.y,pointx,pointy);
            }

            prev.x=pointx;
            prev.y=pointy;
            isFirst=false;
        }

        std::ostringstream dispString;
        dispString << hoverPlace.level;
        
        if(hoverPlace.display){
            circle(hoverPlace.x,hoverPlace.y,5);
            setColors(COLORFLAG_READOUT);
            text(
                hoverPlace.x,hoverPlace.y,
                dispString.str()
            );
        };


        //TODO: of clears every frame :P
        // needRedraw=false;
    }
};
#endif