#ifndef ENVELOPEDOSCILLATOR_H
#define ENVELOPEDOSCILLATOR_H

// #pragma once
#include "../LaneView.h"
#include "../Operator.h"

class Oscillator: public LaneView{
    void __constr_common(){
        frequencyEnvelope=addInput("frequency");
        amplitudeEnvelope=addInput("amplitude");
    }
    public:

    //patcheable module related

    Input* frequencyEnvelope;
    Input* amplitudeEnvelope;

    Oscillator():LaneView(){
        __constr_common();

    }

    Oscillator(const std::string _name):LaneView(_name){
        __constr_common();
    };


    

    //output related
    

    float lastPhase = 0;
    float lastTime=0;

    //fill buffer with output audio. It causes side effects, so that it
    //is possible to get portions of a buffer. 
    //draw thread has to always restart before and after finishing the draw
    //otherwise it will affect the audio output because it did offset the phase
    //drawing also has to be locked while it is being played because otherwise
    //it would restart the waveform on every redraw.
    //this can be solved in the future by multiplying the instances of the 
    //operator: one for draw, another for sound.
    float audio_calculateSample(unsigned long sampleNumber){

        if(!frequencyEnvelope) return 0;
        if(!amplitudeEnvelope) return 0;
        float time = (float) 1000 * sampleNumber / sampleRate;

        if(sampleNumber==0){
            lastTime=0;
            lastPhase=0;
        }

        //realtime approcah, always recalculates inputs
        // float frequency = frequencyEnvelope->audio_calculateSample(sampleNumber);
        // float amplitude = amplitudeEnvelope->audio_calculateSample(sampleNumber);
        //cached approach, reads from input's cached values
        float frequency = frequencyEnvelope->audio_getSample(sampleNumber);
        float amplitude = amplitudeEnvelope->audio_getSample(sampleNumber);

        // std::cout<<"spl y"<<frequency<<"\t"<<amplitude<<std::endl;

        float deltaTime = time - lastTime;
        float phase = lastPhase + (frequency * deltaTime);
        
        if(isnan(phase)) phase = 0;

        lastPhase=phase;
        lastTime=time;
        
        return (float) cos(M_PI_2 * phase / 1000) * amplitude;
    }

    //gui related


    void setCanvas(int x1, int y1, int w, int h)override{
        LaneView::setCanvas(x1,y1,w,h);

        // pixelsPerLevel = canvas.h / 2.00;
    }
    void mouseTargetStart(int x, int y){
        markForRecalculation();
    }

    void draw()override{
        LaneView::draw();

        levelOffset =  canvas.h / (pixelsPerLevel * 2);

        if(!maximized_cached) return;
        drawCache();
    }
};

#endif