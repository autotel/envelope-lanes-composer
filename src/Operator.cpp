#include "Operator.h"
#include <math.h>
std::mutex Operator::audioMutex;
unsigned long Operator::sampleRate = 44100;
double Operator::samplePeriod = 1000.00 / Operator::sampleRate;

void Operator::markForRecalculation(){
    needsRecalculation=true;
    for (
        auto it = outputs.begin(); 
        it != outputs.end(); 
        ++it
    ) {
        Input * theInput = (*it);
        if(!theInput) return;
        Operator & theReceptor = theInput->owner;
        //this prevents infinite loops
        if(!theReceptor.needsRecalculation){
            theReceptor.markForRecalculation();
        }
    }
}
void Operator::connectTo(Input *subject){
    if(!subject) return;
    outputs.insert(subject);
    subject->connectedTo(this);
}
void Operator::disconnect(Input *subject){
    if(!subject) return;
    outputs.erase(subject);
    subject->disconnected(this);
}

void Operator::connectTo(Operator *subject,const std::string inputName){
    connectTo(subject->inputs[inputName]);
}
void Operator::disconnect(Operator *subject,const std::string inputName){
    disconnect(subject->inputs[inputName]);
}

Input* Operator::addInput(
    const std::string friendlyName
){
    Input *theInput=new Input(*this,friendlyName);
    inputs[friendlyName]=theInput;
    return theInput;
}

void Operator::audio_restart(){
    audio_bufferPosition=0;
    //also restart things connected to my input
    //not needed if playing from cache.
    // for (auto const& itr : inputs){
    //     if(itr.second){
    //         Input & theInput = *itr.second;
    //         if(theInput.myConnection){
    //             theInput.myConnection->audio_restart();
    //         }
    //     }
    // }
}

void Operator::audio_fillBuffer(float *buffer,int numSamples){
    unsigned long maxSample=cache.size();
    for(int i=0; i<numSamples; i++){
        if(audio_bufferPosition>maxSample){
            *(buffer+i)=0;
        }else{
            *(buffer+i)=cache[audio_bufferPosition];
        }
        audio_bufferPosition+=1;
    }
};

void Input::connectedTo(Operator *subject){
    myConnection=subject;
}
void Input::disconnected(Operator *subject){
    myConnection=0;
}

//get single sample
//calculate a sample fresh
float Input::audio_calculateSample(unsigned long sampleNumber){
    if(myConnection){
        return myConnection->audio_calculateSample(sampleNumber);
    }
    return 0;
}
//get single sample
//use sample from cache
float Input::audio_getSample(unsigned long sampleNumber){
    if(myConnection){
        myConnection->audio_syncCache();
        return myConnection->cache[sampleNumber];
    }
    return 0;
}
