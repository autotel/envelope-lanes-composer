#ifndef KEY_PRESSED_LINUX_H
#define KEY_PRESSED_LINUX_H
#include <X11/Xlib.h>
#include <iostream>
#include "X11/keysym.h"
namespace keys{
    /**
     *
     * @param ks  like XK_Shift_L, see /usr/include/X11/keysymdef.h
     * @return
     */
    bool is_pressed(KeySym ks);

    bool ctrl_is_pressed();

    bool shift_is_pressed();

};
#endif