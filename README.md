## problems now and next

### operator's history problem: 

one generator has to calculate all the samples from time zero in order to know the value at any given time (sample). For example, an oscillator needs to know the phase it had the last time, or otherwise it will restart every request to fill the buffer.

now I am accumulating values under the assumption that the whole timeline of the operator will be calculated sequentially without any other actor doing this in between. For this, drawing has to be blocked every time the sound plays. This has the grave defects of
 * cannot draw and play at the same time unless every operator is duplicated
 * one operator cannot output to two other operators, otherwise it will advance double speed

one approach to solve this is to pre-calculate the buffer but this would lead to values being unchangeable while playing (won't take effect or cause a glitch)


## ideas
- caches waveforms so that it's less resources
- can be triggered, by midi perhaps,
    - it would need "tabs" ui for multitimbrality. 
    - for poliphony, it would either need to multiply the synth tree per voice, or produce the whole sound buffer at the moment of triggering. 
        - advantage of the first approach: allows sound modulations after being triggered, allows infinite or very long sounds
        - disadvantage of the first approach: amount of voices is set
        - advantage of the second approach: very large amount of voices as it an be arrayed
        - none of the advantages mentioned for the other approach. 
    - i more enthusiasted about multitimbrality than poliphony. Actually it can be easily achieved if the app does not take exclusive control of the control signals (eg. midi) nor audio, and it lets select a subset of those control signals (e.g. respond only to one midi channel)

- midi input, creates a frequency output
- kernel: multiplies input with a 1d kernel (if long enough it can enhance certain frequencies for example, if short it can get the delta)
- adsr
- optional looping points in envelope (actually the envelope could 
optionally be an adsr)
- lpf statistical or/and audio that can be applied to envelopes lanes too
- arithmetic module, which lets inputs be mixed using *, /, +, -. An input can be defined to be a static number
- wave rotator
- looper, by setting a range the incoming signal gets looped, the rest discarded.


